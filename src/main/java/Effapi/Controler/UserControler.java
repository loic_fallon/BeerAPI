package Effapi.Controler;

import Effapi.Model.Beer;
import Effapi.Model.Token;
import Effapi.Model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import javax.annotation.Resource;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static javax.crypto.Cipher.SECRET_KEY;

public class UserControler {

    @PersistenceContext
    private EntityManager entityManager;

    @Resource
    UserTransaction userTransaction;

    protected final Logger LOGGER = Logger.getLogger(getClass().getName());

    Base64.Decoder decoder = Base64.getDecoder();

    public boolean createUser(User user){
        try {
            userTransaction.begin();
            entityManager.persist(user);
            userTransaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "JPA Error" + e.getMessage());
            return false;
        }
    }

    public String loginUser(User user){
        String token = "";
        try {
            Query query = entityManager.createQuery("select u from User u where u.username = :usnme", User.class);
            query.setParameter("usnme", user.username);
            User userEntity = (User) query.getSingleResult();
            if (userEntity.password.equals(user.password)){
                token = generateToken(user);
            }
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "JPA Error" + e.getMessage());
        }
        return token;
    }

    public void isLogged(String token) throws InvalidJWTToken {
        try{
            String cleanedToken = token.replace("Bearer", "").trim();
            if (!cleanedToken.isEmpty()){
//                Claims jwt = decryptToken(cleanedToken);
//                LOGGER.log(Level.SEVERE, "End decrypt JWT");

                List<Token> blackList = getBlacklist();
                Stream<Token> blacklistedToken = blackList.stream().filter(entityToken -> entityToken.token.equals(cleanedToken));
                if (blacklistedToken.count() > 0){
                    throw new InvalidJWTToken("User logout");
                }
            }
        }catch (Exception e){
            throw new InvalidJWTToken("Error during geneterating token");
        }
    }

    public boolean logout(String token) {
        String cleanedToken = token.replace("Bearer", "").trim();
        setBlacklistToken(cleanedToken);
        return true;
    }

    private String generateToken(User user) {

        String key = new String(decoder.decode("SECRET"));

        return Jwts.builder()
                .claim("name", user.id)
                .setSubject("BeerAPI")
                .setId(UUID.randomUUID().toString())
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(5L, ChronoUnit.MINUTES)))
                .compact();
    }

    private List<Token> getBlacklist() {
        return entityManager.createQuery("select tk from Token tk", Token.class).getResultList();
    }

    private void setBlacklistToken(String token) {
        try {
            Token entityToken = new Token();
            entityToken.token = token;

            userTransaction.begin();
            entityManager.persist(entityToken);
            userTransaction.commit();
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "JPA Error" + e.getMessage());
        }
    }

    private Claims decryptToken(String token) {
        LOGGER.log(Level.SEVERE, "Start decrypt JWT");
        return Jwts.parserBuilder()
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public static class InvalidJWTToken extends Exception {
        public InvalidJWTToken(String errorMessage) {
            super((errorMessage));
        }
    }
}
