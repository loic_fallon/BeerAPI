package Effapi.Controler;

import Effapi.Model.Beer;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BeerControler {

    @PersistenceContext
    private EntityManager entityManager;

    @Resource
    UserTransaction userTransaction;


    public List<Beer> getBeer() {
        return entityManager.createQuery("select br from Beer br", Beer.class).getResultList();
    }

    public boolean addBeer(Beer beer) {
        try {
            userTransaction.begin();
            entityManager.persist(beer);
            userTransaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "JPA Error" + e.getMessage());
            return false;
        }
    }

    public boolean updateBeer(Beer beer) {
        try {
            userTransaction.begin();
            Beer beerEntity = entityManager.find(Beer.class, beer.getId());

            if(beerEntity == null)
                return false;

            beerEntity.setName(beer.getName());
            beerEntity.setType(beer.getType());
            beerEntity.setDegree(beer.getDegree());

            entityManager.persist(beerEntity);
            userTransaction.commit();
            return true;
        }catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "JPA Error" + e.getMessage());
            return false;
        }
    }

    public boolean deleteBeer(Beer beer) {
        try {
            userTransaction.begin();
            Beer mangaEntity = entityManager.find(Beer.class, beer.getId());

            if(mangaEntity == null)
                return false;

            entityManager.remove(mangaEntity);
            userTransaction.commit();
            return true;
        }catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "JPA Error" + e.getMessage());
            return false;
        }
    }

//    public boolean getCredentials(String userAgent) {
//        String base64Credentials = userAgent.substring("Basic".length()).trim();
//        byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
//        String credentials = new String(credDecoded, StandardCharsets.UTF_8);
//
//        // credentials = username:password
//        final String[] values = credentials.split(":", 2);
//
//        var test = entityManager.createQuery("select log from Token log", Token.class).getResultList();
//
//        String login = values[0];
//        String password = values[1];
//        if(!Objects.equals(login, test.get(0).Login) || !Objects.equals(test.get(0).Token, password)) {
//            String uuid = UUID.randomUUID().toString();
//            System.out.println("uuid = " + uuid);
//            return false;
//        }
//
//        return true;
//    }
}
