package Effapi;
import Effapi.Controler.BeerControler;
import Effapi.Controler.UserControler;
import Effapi.Model.Beer;
import Effapi.Model.User;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("beers")
public class SampleResource {

	@Inject
	private BeerControler beerControler;

	@Inject
	private UserControler userControler;

	protected final Logger LOGGER = Logger.getLogger(getClass().getName());

	@HeaderParam("Authorization") String userAgent;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBeer() {
		Response.ResponseBuilder builder;
		try {
			userControler.isLogged(userAgent);

			List<Beer> beerList = beerControler.getBeer();
			if (!beerList.isEmpty()){
				//todo ENLEVER TOJson
				builder = Response.ok().entity(beerList);
			}
			else{
				builder = Response.status(204);
			}

		}
		catch (UserControler.InvalidJWTToken e){
			builder = Response.status(401);
		}
		catch (Exception e){
			builder = Response.status(400);
		}
		return builder.build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response createBeer(String beer) {
		Response.ResponseBuilder builder;
		try {
			userControler.isLogged(userAgent);
			boolean isAdded = beerControler.addBeer(getBeerEntity(beer));
			if (isAdded){
				builder = Response.ok();
			}
			else {
				builder = Response.status(500);
			}


		}
		catch (UserControler.InvalidJWTToken e){
			builder = Response.status(401);
		}
		catch (Exception e){
			builder = Response.status(400);
		}
		return builder.build();
	}

	//todo inverser post et put
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response updateBeer(String beer) {
		Response.ResponseBuilder builder;
		try {
			userControler.isLogged(userAgent);
			boolean isUpdate = beerControler.updateBeer(getBeerEntity(beer));
			if (isUpdate){
				builder = Response.ok();
			}
			else {
				builder = Response.status(500);
			}

		}
		catch (UserControler.InvalidJWTToken e){
			builder = Response.status(401);
		}
		catch (Exception e){
			builder = Response.status(400);
		}
		return builder.build();
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteBeer(String beer){
		Response.ResponseBuilder builder;
		try {
			userControler.isLogged(userAgent);
			boolean isDelete = beerControler.deleteBeer(getBeerEntity(beer));
			if (isDelete){
				builder = Response.ok();
			} else {
				builder = Response.status(500);
			}

		}
		catch (UserControler.InvalidJWTToken e){
			builder = Response.status(401);
		}
		catch (Exception e){
			builder = Response.status(400);
		}
		return builder.build();
	}

	@PUT
	@Path("enrollment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response createUser(String user) {
		Response.ResponseBuilder builder;
		try {
			boolean isAdded = userControler.createUser(getUserEntity(user));
			if (isAdded){
				builder = Response.ok();
			}
			else {
				builder = Response.status(500);
			}
		} catch (Exception e){
			builder = Response.status(500);
		}
		return builder.build();
	}

	@GET
	@Path("login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response loginUser(String user) {
		Response.ResponseBuilder builder;
		try {
			String userToken = userControler.loginUser(getUserEntity(user));
			LOGGER.log(Level.SEVERE, "Token:" + userToken);
			if (!userToken.equals("")){
				builder = Response.ok(userToken);
			}
			else {
				builder = Response.status(403);
			}
		} catch (Exception e){
			builder = Response.status(500);
		}
		return builder.build();
	}

	@POST
	@Path("logout")
	public Response logoutUser() {
		Response.ResponseBuilder builder;
		try {
			boolean isLogout = userControler.logout(userAgent);
			if (isLogout){
				builder = Response.ok();
			}
			else {
				builder = Response.status(500);
			}
		} catch (Exception e){
			builder = Response.status(500);
		}
		return builder.build();
	}

	public Beer getBeerEntity(String json) throws JSONBADFORMAT {
		try {
			Gson gson = new Gson();
			return gson.fromJson(json, Beer.class);
		}catch (Exception e){
			Logger.getGlobal().log(Level.SEVERE, "JPA Error" + e.getMessage());
			throw new JSONBADFORMAT("INCORRECT OBJECT");
		}
	}

	public User getUserEntity(String json) throws JSONBADFORMAT {
		try {
			Gson gson = new Gson();
			return gson.fromJson(json, User.class);
		}catch (Exception e){
			Logger.getGlobal().log(Level.SEVERE, "JPA Error" + e.getMessage());
			throw new JSONBADFORMAT("INCORRECT OBJECT");
		}
	}

	public String toJson(Object object){
		Gson gson = new Gson();
		return gson.toJson(object);
	}

	static class JSONBADFORMAT extends Exception {
		public JSONBADFORMAT(String errorMessage) {
			super((errorMessage));
		}
	}

}
